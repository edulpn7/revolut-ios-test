import Foundation
import Nimble
import XCTest
@testable import revolut_ios_test

class CurrencyConverterControllerTests: XCTestCase {
    class CurrencyConvertionInteractorMock: CurrencyConvertionInteractor {
        var monitorInvocationCount: Int = 0
        override func monitor() {
            monitorInvocationCount += 1
            output?.output(CurrencyConvertionList(rates: [CurrencyConvertionRate(currency: "anyCurrency", rate: 13.47)]))
        }

        var selectNewCurrencyMock: String?
        var selectInvocationCount: Int = 0
        override func select(_ newCurrency: String, from currentConvertionList: CurrencyConvertionList) {
            selectInvocationCount += 1
            selectNewCurrencyMock = newCurrency
            output?.output(currentConvertionList)
        }

        var updateAmountMock: Decimal?
        var updateInvocationCount: Int = 0
        override func update(_ amount: Decimal) {
            updateInvocationCount += 1
            updateAmountMock = amount
        }
    }

    class CurrencyConverterViewControllerMock: CurrencyConverterViewController {
        var bindInvocationCount: Int = 0
        override func bind(_ viewModel: CurrencyConverterViewModel) {
            bindInvocationCount += 1
        }
    }

    func testStartMonitoring() {
        let interactorMock = CurrencyConvertionInteractorMock()
        let viewControllerMock = CurrencyConverterViewControllerMock()
        let controller = CurrencyConverterController(viewController: viewControllerMock, interactor: interactorMock)

        //Just so compiler won't do any funny stuff releasing the controller
        expect(controller).toNot(beNil())
        expect(interactorMock.monitorInvocationCount) == 1

        //This will happen on another thread, so expecting asynchronously
        expect(viewControllerMock.bindInvocationCount).toEventuallyNot(be(0))
    }

    func testSelectNewCurrency() {
        let interactorMock = CurrencyConvertionInteractorMock()
        let viewControllerMock = CurrencyConverterViewControllerMock()
        let controller = CurrencyConverterController(viewController: viewControllerMock, interactor: interactorMock)

        controller.currentConvertionList = CurrencyConvertionList(rates: [CurrencyConvertionRate(currency: "anyCurrency", rate: 13.47)])
        controller.select("anyCurrency2")

        expect(interactorMock.selectInvocationCount) == 1
        expect(interactorMock.selectNewCurrencyMock).toNot(beNil())
        expect(viewControllerMock.bindInvocationCount).toEventuallyNot(be(0))
    }

    func testUpdateAmount() {
        let interactorMock = CurrencyConvertionInteractorMock()
        let viewControllerMock = CurrencyConverterViewControllerMock()
        let controller = CurrencyConverterController(viewController: viewControllerMock, interactor: interactorMock)

        controller.update("13.47")

        expect(interactorMock.updateInvocationCount) == 1
        expect(interactorMock.updateAmountMock).toNot(beNil())
        expect(viewControllerMock.bindInvocationCount).toEventuallyNot(be(0))
    }
}
