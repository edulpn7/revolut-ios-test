import Foundation
import Nimble
import XCTest
@testable import revolut_ios_test

class CurrencyConvertionInteractorTests: XCTestCase {
    struct AnyError: DisplayableError {}

    class OutputMock: CurrencyConvertionInteractorOutputProtocol {
        var outputErrorMock: DisplayableError?
        var outputErrorMockInvocationCount: Int = 0
        func output(_ error: DisplayableError) {
            outputErrorMockInvocationCount += 1
            outputErrorMock = error
        }

        var outputCurrencyConvertionListMock: CurrencyConvertionList?
        var outputCurrencyConvertionListMockInvocationCount: Int = 0
        func output(_ updatedCurrencyConvertionList: CurrencyConvertionList) {
            outputCurrencyConvertionListMockInvocationCount += 1
            outputCurrencyConvertionListMock = updatedCurrencyConvertionList
        }
    }

    class CurrencyConvertionListRepositoryStub: CurrencyConvertionListRepositoryProtocol {
        var cancelInvocationCount: Int = 0
        func cancel() {
            cancelInvocationCount += 1
        }

        var updated: CurrencyConvertionListEntity?
        var updatedInvocationCount: Int = 0
        func retrieveUpdatedConvertionList(for currency: String,
                                           with completion: @escaping (Result<CurrencyConvertionList>) -> ()) {
            updatedInvocationCount += 1
            if let updated = updated {
                completion(.success(value: CurrencyConvertionList(mapping: updated)))
            } else {
                completion(.failure(error: AnyError()))
            }
        }

        var cache: CurrencyConvertionListEntity?
        var cacheInvocationCount: Int = 0
        func retrieveCurrentConvertionList(with completion: @escaping (Result<CurrencyConvertionList>) -> ()) {
            cacheInvocationCount += 1
            if let cache = cache {
                completion(.success(value: CurrencyConvertionList(mapping: cache)))
            } else {
                completion(.failure(error: AnyError()))
            }
        }
    }

    func testUpdateAmountWithNoCacheThenError() {
        let output = OutputMock()
        let stubRepository = CurrencyConvertionListRepositoryStub()
        stubRepository.cache = nil
        let interactor = CurrencyConvertionInteractor(repository: stubRepository)
        interactor.output = output

        interactor.update(1.0)

        expect(stubRepository.cancelInvocationCount) == 1
        expect(stubRepository.cacheInvocationCount) == 1
        expect(output.outputErrorMockInvocationCount) == 1
        expect(output.outputErrorMock).toNot(beNil())
    }

    func testUpdateAmountWithCacheThenUpdateConvertionList() {
        let output = OutputMock()
        let stubRepository = CurrencyConvertionListRepositoryStub()
        stubRepository.cache = CurrencyConvertionListEntity(base: "anyCurrency",
                                                            date: "anyDate",
                                                            rates: ["anyCurrency2" : 2.0])
        let interactor = CurrencyConvertionInteractor(repository: stubRepository)
        interactor.output = output

        interactor.update(3.0)

        expect(stubRepository.cancelInvocationCount) == 1
        expect(stubRepository.cacheInvocationCount) == 1
        expect(output.outputErrorMockInvocationCount) == 0
        expect(output.outputErrorMock).to(beNil())
        expect(output.outputCurrencyConvertionListMockInvocationCount) == 1
        expect(output.outputCurrencyConvertionListMock).toNot(beNil())
        expect(output.outputCurrencyConvertionListMock!.rates[0].currency) == "anyCurrency"
        expect(output.outputCurrencyConvertionListMock!.rates[0].rate) == 3.0
        expect(output.outputCurrencyConvertionListMock!.rates[1].currency) == "anyCurrency2"
        expect(output.outputCurrencyConvertionListMock!.rates[1].rate) == 6.0
    }

    func testMonitorWithSuccessThenReturnConvertionList() {
        let output = OutputMock()
        let stubRepository = CurrencyConvertionListRepositoryStub()
        stubRepository.updated = CurrencyConvertionListEntity(base: "EUR",
                                                              date: "anyDate",
                                                              rates: ["anyCurrency" : 2.0])
        let interactor = CurrencyConvertionInteractor(repository: stubRepository)
        interactor.output = output

        interactor.monitor()

        expect(output.outputCurrencyConvertionListMockInvocationCount).toEventually(be(1), timeout: 1.1)
        expect(output.outputCurrencyConvertionListMock).toEventuallyNot(beNil(), timeout: 1.1)
        expect(stubRepository.updatedInvocationCount).toEventually(be(1), timeout: 1.1)

        waitUntil(timeout: 1.1) { done in
            if let list = output.outputCurrencyConvertionListMock {
                expect(list.rates[0].rate) == 100.0
                expect(list.rates[0].currency) == "EUR"
                expect(list.rates[1].rate) == 200.0
                expect(list.rates[1].currency) == "anyCurrency"
                done()
            }
        }
    }

    func testMonitorWithFailureThenError() {
        let output = OutputMock()
        let stubRepository = CurrencyConvertionListRepositoryStub()
        stubRepository.updated = nil
        let interactor = CurrencyConvertionInteractor(repository: stubRepository)
        interactor.output = output

        interactor.monitor()

        expect(output.outputErrorMockInvocationCount).toEventually(be(1), timeout: 1.1)
        expect(output.outputErrorMock).toEventuallyNot(beNil(), timeout: 1.1)
        expect(stubRepository.updatedInvocationCount).toEventually(be(1), timeout: 1.1)
    }

    func testSelectNewCurrency() {
        let output = OutputMock()
        let currentConvertionRates = [CurrencyConvertionRate(currency: "anyCurrency", rate: 10.0), CurrencyConvertionRate(currency: "anyCurrency2", rate: 13.47)]
        let currentConvertionList = CurrencyConvertionList(rates: currentConvertionRates)

        let stubRepository = CurrencyConvertionListRepositoryStub()
        let interactor = CurrencyConvertionInteractor(repository: stubRepository)
        interactor.output = output

        interactor.select("anyCurrency2", from: currentConvertionList)

        expect(stubRepository.cancelInvocationCount) == 1
        expect(output.outputCurrencyConvertionListMock!.rates[0].currency) == "anyCurrency2"
        expect(output.outputCurrencyConvertionListMock!.rates[0].rate) == 13.47
        expect(output.outputCurrencyConvertionListMock!.rates[1].currency) == "anyCurrency"
        expect(output.outputCurrencyConvertionListMock!.rates[1].rate) == 10.0

        expect(output.outputCurrencyConvertionListMockInvocationCount).toEventually(be(1), timeout: 1.1)
        expect(output.outputCurrencyConvertionListMock).toEventuallyNot(beNil(), timeout: 1.1)
        expect(stubRepository.updatedInvocationCount).toEventually(be(1), timeout: 1.1)
    }
}
