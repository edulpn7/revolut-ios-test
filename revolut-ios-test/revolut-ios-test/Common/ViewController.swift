import UIKit

protocol ViewModel { }

class View: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    required init() {
        super.init(frame: CGRect.zero)
        self.setup()
    }

    func initialize() {}

    func installConstraints() {}

    private func setup() {
        self.initialize()
        self.installConstraints()
    }
}

class ViewController<ViewType: View, ViewModelType: ViewModel>: UIViewController {
    var typedView: ViewType {
        guard let typedView = self.view as? ViewType else {
            fatalError("typedView should be of type" + String(describing: ViewType.self))
        }
        return typedView
    }

    override func loadView() {
        let view = ViewType()
        view.bounds = CGRect(origin: .zero, size: UIScreen.main.bounds.size)
        self.view = view
    }

    func bind(_ viewModel: ViewModelType) { }

    public init() {
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
