import UIKit

protocol ReusableView {
    static var reuseIdentifier: String {get}
}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func register<T: UITableViewCell>(_ cellClass: T.Type) where T: ReusableView {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue reusable cell for identifier " + T.reuseIdentifier)
        }
        return cell
    }

    func cellForRow<T: UITableViewCell>(at indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.cellForRow(at: indexPath) as? T else {
            fatalError("Could not get a cell of type " + String(describing: T.self) + "at the provided index path")
        }
        return cell
    }
}
