import UIKit
import Cartography

protocol ActivityIndicatorPresenter {
    var activityIndicatorView: UIView? {get set}
    func startActivity()
    func stopActivity()
}

extension ActivityIndicatorPresenter where Self: UIViewController {
    func startActivity() {
        setupIndicatorView(isHidden: false)
    }

    func stopActivity() {
        setupIndicatorView(isHidden: true)
    }

    private func setupIndicatorView(isHidden: Bool) {
        if let activityIndicatorView = activityIndicatorView {
            if !view.subviews.contains(activityIndicatorView) {
                view.addSubview(activityIndicatorView)
                constrain(activityIndicatorView) {
                    $0.edges == $0.superview!.safeAreaIfAvailableLayoutGuide.edges
                }
            }
            activityIndicatorView.isHidden = isHidden
            isHidden ? view.sendSubviewToBack(activityIndicatorView) : view.bringSubviewToFront(activityIndicatorView)
        }
    }
}

class LoadingView: UIView {
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.startAnimating()
        addSubview(activityIndicator)
        constrain(activityIndicator) {
            $0.center == $0.superview!.center
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
