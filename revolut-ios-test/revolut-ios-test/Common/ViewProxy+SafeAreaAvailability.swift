import Cartography

extension ViewProxy {
    var safeAreaIfAvailableLayoutGuide: LayoutGuideProxy {
        if #available(iOS 11, *) {
            return safeAreaLayoutGuide
        } else {
            return layoutMarginsGuide
        }
    }
}
