import Foundation

class FormatterProvider {
    static let shared = FormatterProvider()
    private lazy var numberFormatter = NumberFormatter()

    private init() {}

    func string(from decimalNumber: Decimal, fractionDigits: UInt = 2) -> String {
        numberFormatter.maximumFractionDigits = Int(fractionDigits)
//        numberFormatter.minimumFractionDigits = Int(fractionDigits)
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ""
        return numberFormatter.string(from: NSDecimalNumber(decimal: decimalNumber)) ?? ""
    }
}
