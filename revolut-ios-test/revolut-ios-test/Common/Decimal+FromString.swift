import Foundation

extension Decimal {
    init(string: String?) {
        if let string = string {
            if let decimalFromString = Decimal(string: string) {
                self = decimalFromString
            } else {
                self = 0.0
            }
        } else {
            self = 0.0
        }
    }
}

