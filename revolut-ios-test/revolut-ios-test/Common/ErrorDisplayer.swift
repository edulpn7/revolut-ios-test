import UIKit

protocol DisplayableError: Error {
    var displayableMessage: String {get}
}

extension DisplayableError {
    var displayableMessage: String {
        return "Something wrong happened, please try again later"
    }
}

protocol ErrorDisplayer {
    func display(_ error: DisplayableError)
}

extension ErrorDisplayer where Self: UIViewController {
    func display(_ error: DisplayableError) {
        let alert = UIAlertController(title: nil, message: error.displayableMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) { _ in })
        present(alert, animated: true)
    }
}
