import Foundation

extension Array {
    func appended(by element: Element) -> Array {
        return self + [element]
    }

    func prepended(by element: Element) -> Array {
        return [element] + self
    }

    func swappedAt(_ i: Int, _ j: Int) -> Array {
        var swappedSelf = self
        swappedSelf.swapAt(i, j)
        return swappedSelf
    }
}
