import Foundation

enum Result<T> {
    case success(value: T)
    case failure(error: DisplayableError)
}
