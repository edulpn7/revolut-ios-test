import Foundation

struct CurrencyConvertionList {
    let rates: [CurrencyConvertionRate]
}

struct CurrencyConvertionRate {
    let currency: String
    let rate: Decimal
}
