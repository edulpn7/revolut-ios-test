import Foundation

protocol CurrencyConvertionInteractorOutputProtocol: class {
    func output(_ updatedCurrencyConvertionList: CurrencyConvertionList)
    func output(_ error: DisplayableError)
}

class CurrencyConvertionInteractor {
    weak var output: CurrencyConvertionInteractorOutputProtocol?
    var timer: Timer?
    let repository: CurrencyConvertionListRepositoryProtocol

    init(repository: CurrencyConvertionListRepositoryProtocol = CurrencyConvertionListRepository.shared) {
        self.repository = repository
    }

    deinit {
        timer?.invalidate()
        timer = nil
    }

    func monitor() {
        monitor("EUR", for: 100.0)
    }

    func select(_ newCurrency: String, from currentConvertionList: CurrencyConvertionList) {
        timer?.invalidate()
        repository.cancel()

        let newCurrencyIndex = currentConvertionList.rates.firstIndex { $0.currency == newCurrency } ?? 0
        let newCurrencyRate = currentConvertionList.rates[newCurrencyIndex].rate
        let swappedRates = currentConvertionList.rates.swappedAt(newCurrencyIndex, 0)
        let swappedList = CurrencyConvertionList(rates: swappedRates)
        output?.output(swappedList)
        monitor(newCurrency, for: newCurrencyRate)
    }

    func update(_ amount: Decimal) {
        timer?.invalidate()
        repository.cancel()

        repository.retrieveCurrentConvertionList { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let currentConvertionList):
                let rateRelation = amount / currentConvertionList.rates[0].rate
                let updatedRates = currentConvertionList.rates.map {
                    CurrencyConvertionRate(currency: $0.currency, rate: $0.rate * rateRelation)
                }
                let updatedList = CurrencyConvertionList(rates: updatedRates)
                strongSelf.output?.output(updatedList)

                let newCurrencyRate = updatedRates[0]
                strongSelf.monitor(newCurrencyRate.currency, for: newCurrencyRate.rate)
            case .failure(let error):
                strongSelf.output?.output(error)
            }
        }
    }

    private func monitor(_ currency: String, for amount: Decimal) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            strongSelf.retrieveConvertionList(for: currency, and: amount)
        }
    }

    private func retrieveConvertionList(for currency: String, and amount: Decimal) {
        repository.retrieveUpdatedConvertionList(for: currency) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            switch $0 {
            case .success(let convertionList):
                let updatedRates = convertionList.rates.map {
                    CurrencyConvertionRate(currency: $0.currency, rate: $0.rate * amount)
                }
                let updatedConvertionList = CurrencyConvertionList(rates: updatedRates)
                strongSelf.output?.output(updatedConvertionList)
            case .failure(let error):
                strongSelf.output?.output(error)
                strongSelf.timer?.invalidate()
            }
        }
    }
}
