import Foundation

struct CurrencyConvertionListEntity: Decodable {
    let base: String
    let date: String
    let rates: [String: Double]
}
