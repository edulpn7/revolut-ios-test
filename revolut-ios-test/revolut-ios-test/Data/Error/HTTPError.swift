enum HTTPError: DisplayableError {
    case badRequest
    case unauthorized
    case forbidden
    case notFound
    case unknown
    //...

    static func mapping(statusCode: Int) -> HTTPError {
        switch statusCode {
        case 400:
            return .badRequest
        case 401:
            return .unauthorized
        case 403:
            return .forbidden
        case 404:
            return .notFound
        //...
        default:
            return .unknown
        }
    }
}
