import Foundation

enum JSONDecodingError: DisplayableError {
    case unableToDecode
}
