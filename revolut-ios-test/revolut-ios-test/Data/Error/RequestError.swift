import Foundation

enum RequestError: DisplayableError {
    case invalid
    case unknown
}
