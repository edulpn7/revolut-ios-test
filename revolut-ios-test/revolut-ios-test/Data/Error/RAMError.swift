import Foundation

enum RAMError: DisplayableError {
    case unavailable
}
