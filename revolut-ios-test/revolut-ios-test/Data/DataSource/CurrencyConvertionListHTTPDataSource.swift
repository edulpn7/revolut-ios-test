import Foundation

class CurrencyConvertionListHTTPDataSource {
    func retrieveConvertionList(for currency: String,
                                with completion: @escaping (Result<CurrencyConvertionListEntity>) -> ()) {
        if var urlComponents = URLComponents(string: "https://revolut.duckdns.org/latest") {
            urlComponents.queryItems = [URLQueryItem(name: "base", value: currency)]
            if let url = urlComponents.url {
                let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200, let data = data {
                            if let entity = try? JSONDecoder().decode(CurrencyConvertionListEntity.self, from: data) {
                                completion(.success(value: entity))
                            } else {
                                completion(.failure(error: JSONDecodingError.unableToDecode))
                            }
                        } else {
                            completion(.failure(error: HTTPError.mapping(statusCode: httpResponse.statusCode)))
                        }
                    } else {
                        completion(.failure(error: RequestError.unknown))
                    }
                }
                task.resume()
            }
        } else {
            completion(.failure(error: RequestError.invalid))
        }
    }
}
