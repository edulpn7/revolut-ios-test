import Foundation

protocol RAMDataSource: class {
    associatedtype T
    var value: T? {get set}
}
