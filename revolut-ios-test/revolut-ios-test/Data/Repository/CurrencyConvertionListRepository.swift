import Foundation

protocol CurrencyConvertionListRepositoryProtocol  {
    func retrieveCurrentConvertionList(with completion: @escaping (Result<CurrencyConvertionList>) -> ())
    func retrieveUpdatedConvertionList(for currency: String,
                                       with completion: @escaping (Result<CurrencyConvertionList>) -> ())
    func cancel()
}

class CurrencyConvertionListRepository: CurrencyConvertionListRepositoryProtocol {
    let dataSource: CurrencyConvertionListHTTPDataSource
    let ramDataSource: CurrencyConvertionListRAMDataSource
    var shouldCancel: Bool = false

    static let shared = CurrencyConvertionListRepository()

    private init(dataSource: CurrencyConvertionListHTTPDataSource = .init(),
                 ramDataSource: CurrencyConvertionListRAMDataSource = .init()) {
        self.dataSource = dataSource
        self.ramDataSource = ramDataSource
    }

    func retrieveCurrentConvertionList(with completion: @escaping (Result<CurrencyConvertionList>) -> ()) {
        if let cachedEntity = ramDataSource.value {
            let model = CurrencyConvertionList(mapping: cachedEntity)
            completion(.success(value: model))
        } else {
            completion(.failure(error: RAMError.unavailable))
        }
    }

    func cancel() {
        shouldCancel = true
    }

    func retrieveUpdatedConvertionList(for currency: String,
                                       with completion: @escaping (Result<CurrencyConvertionList>) -> ()) {
        dataSource.retrieveConvertionList(for: currency) { [weak self] result in
            guard let strongSelf = self else { return }
            if !strongSelf.shouldCancel {
                switch result {
                case .success(let entity):
                    strongSelf.ramDataSource.value = entity
                    let model = CurrencyConvertionList(mapping: entity)
                    completion(.success(value: model))
                case .failure(let error):
                    completion(.failure(error: error))
                }
            }
            strongSelf.shouldCancel = false
        }
    }
}

extension CurrencyConvertionList {
    init(mapping entity: CurrencyConvertionListEntity) {
        rates = entity.rates
            .map { CurrencyConvertionRate(currency: $0, rate: Decimal($1)) }
            .prepended(by: CurrencyConvertionRate(currency: entity.base, rate: Decimal(1.00)))
    }
}
