enum CurrencyConverterViewModel: ViewModel {
    case success(viewModels: [CurrencyConverterSuccessViewModel])
    case failure(error: DisplayableError)
}

struct CurrencyConverterSuccessViewModel {
    let currency: String
    let amount: String
}
