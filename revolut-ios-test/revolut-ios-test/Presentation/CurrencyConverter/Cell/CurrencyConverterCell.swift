import UIKit
import Cartography

class CurrencyConverterCell: UITableViewCell, ReusableView {
    let currencyLabel = UILabel()
    let rateLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(currencyLabel)
        contentView.addSubview(rateLabel)
        constrain(currencyLabel, rateLabel) {
            $0.left == $0.superview!.left + 10
            $0.centerY == $0.superview!.centerY
            $1.centerY == $0.centerY
            $1.right == $0.superview!.right - 10
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configure(with currencyText: String, and rateText: String, isEditing: Bool) {
        currencyLabel.text = currencyText
        rateLabel.text = rateText
        rateLabel.textColor = (isEditing ? .red : .black)
    }
}
