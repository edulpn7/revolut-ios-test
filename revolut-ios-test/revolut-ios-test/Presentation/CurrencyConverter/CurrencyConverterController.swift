import Foundation

class CurrencyConverterController {
    let viewController: CurrencyConverterViewController
    var interactor: CurrencyConvertionInteractor
    var currentConvertionList: CurrencyConvertionList?

    init(viewController: CurrencyConverterViewController = .init(),
         interactor: CurrencyConvertionInteractor = .init()) {
        self.viewController = viewController
        self.interactor = interactor

        self.viewController.controller = self
        self.interactor.output = self
        
        interactor.monitor()
        viewController.startActivity()
    }

    func select(_ newCurrency: String) {
        if let currentConvertionList = currentConvertionList {
            interactor.select(newCurrency, from: currentConvertionList)
        }
    }

    func update(_ amount: String?) {
        interactor.update(Decimal(string: amount))
    }
}

extension CurrencyConverterController: CurrencyConvertionInteractorOutputProtocol {
    func output(_ error: DisplayableError) {
        DispatchQueue.main.async { [viewController] in
            viewController.stopActivity()
            viewController.bind(.failure(error: error))
        }
    }

    func output(_ updatedCurrencyConvertionList: CurrencyConvertionList) {
        currentConvertionList = updatedCurrencyConvertionList
        DispatchQueue.main.async { [viewController] in
            viewController.stopActivity()
            viewController.bind(CurrencyConverterViewModel.mapping(updatedCurrencyConvertionList))
        }
    }
}

extension CurrencyConverterViewModel {
    static func mapping(_ model: CurrencyConvertionList) -> CurrencyConverterViewModel {
        let viewModels = model.rates
            .map { CurrencyConverterSuccessViewModel(currency: $0.currency,
                                                     amount: FormatterProvider.shared.string(from: $0.rate))}
        return .success(viewModels: viewModels)
    }
}
