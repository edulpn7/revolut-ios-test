import Cartography
import UIKit

class CurrencyConverterView: View {
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.rowHeight = 70.0
        return tableView
    }()
    lazy var rateInputTextField: UITextField = {
        let textField = UITextField()
        textField.keyboardType = .decimalPad
        return textField
    }()

    override func initialize() {
        backgroundColor = .white
        addSubview(rateInputTextField)
        addSubview(tableView)
        sendSubviewToBack(rateInputTextField)
    }

    override func installConstraints() {
        constrain(tableView, rateInputTextField) {
            $1.top == $1.superview!.safeAreaIfAvailableLayoutGuide.top
            $1.left == $1.superview!.safeAreaIfAvailableLayoutGuide.left
            $1.right == $1.superview!.safeAreaIfAvailableLayoutGuide.right
            $0.edges == $0.superview!.safeAreaIfAvailableLayoutGuide.edges
        }
    }
}
