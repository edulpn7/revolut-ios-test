import UIKit

class CurrencyConverterViewController: ViewController<CurrencyConverterView, CurrencyConverterViewModel>, UITableViewDelegate, UITableViewDataSource, ErrorDisplayer, ActivityIndicatorPresenter {
    var activityIndicatorView: UIView? = LoadingView()
    weak var controller: CurrencyConverterController?

    override func viewDidLoad() {
        typedView.tableView.dataSource = self
        typedView.tableView.delegate = self
        typedView.tableView.register(CurrencyConverterCell.self)
        typedView.rateInputTextField.addTarget(self,
                                               action: #selector(textFieldDidChangeText(_:)),
                                               for: .editingChanged)
    }

    private var viewModels: [CurrencyConverterSuccessViewModel] = []
    override func bind(_ viewModel: CurrencyConverterViewModel) {
        switch viewModel {
        case .success(let viewModels):
            self.viewModels = viewModels
            typedView.tableView.reloadData()
        case .failure(let error):
            viewModels = []
            typedView.tableView.reloadData()
            display(error)
        }
    }

    @objc func textFieldDidChangeText(_ sender: UITextField) {
        controller?.update(typedView.rateInputTextField.text)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyConverterCell = tableView.dequeReusableCell(for: indexPath)
        let viewModel = viewModels[indexPath.row]
        let isEditing = (indexPath.row == 0) && typedView.rateInputTextField.isFirstResponder
        cell.configure(with: viewModel.currency,
                       and: viewModel.amount,
                       isEditing: isEditing)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        typedView.rateInputTextField.resignFirstResponder()
        let firstRowIndexPath = IndexPath(row: 0, section: 0)
        if indexPath != firstRowIndexPath {
            let selectedViewModel = viewModels[indexPath.row]

            CATransaction.begin()
            CATransaction.setCompletionBlock { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.controller?.select(selectedViewModel.currency)
                strongSelf.typedView.rateInputTextField.text = selectedViewModel.amount
                strongSelf.typedView.rateInputTextField.becomeFirstResponder()
            }
            tableView.moveRow(at: indexPath, to: firstRowIndexPath)
            tableView.scrollToRow(at: firstRowIndexPath, at: .bottom, animated: true)

            CATransaction.commit()
        } else {
            let selectedViewModel = viewModels[firstRowIndexPath.row]
            typedView.rateInputTextField.text = selectedViewModel.amount
            typedView.rateInputTextField.becomeFirstResponder()
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        typedView.rateInputTextField.resignFirstResponder()
    }
}
